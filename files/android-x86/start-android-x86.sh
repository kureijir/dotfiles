#!/bin/bash

##### IMAGES PATH #####
DIR=$(dirname $0)
HDD_IMG=${DIR}'/android-x86-oreo-rc2.img'
CD_IMG=${DIR}'/android-x86_64-8.1-rc2.iso'

##### VM DEFAULT OPTIONS #####
RAM=2G
SMP=2

##### VM OPTIONS #####
#RAM=2G
#SMP=4
SOUNDHW=ac97                        # available options: hda ac97 es1370 adlib sb16
#DISPLAY=gtk                         # available options: gtk vnc sdl curses
# use virtio vga driver to enable hardware acceleration
VGA=virtio                          # available options: virtio cg3 tcx qxl vmware std cirrus

# building qemu option string
OPTIONS='-enable-kvm'' -m '${RAM}' -smp '${SMP}' -net nic -net user -hda '${HDD_IMG}
if [[ -n $SOUNDHW ]]; then
  OPTIONS=${OPTIONS}' -soundhw '${SOUNDHW}
fi
if [[ $VGA = 'virtio' ]]; then
  OPTIONS=${OPTIONS}' -vga virtio -display gtk,gl=on'
elif [[ -n $VGA ]]; then
  OPTIONS=${OPTIONS}' -vga '${VGA}
fi


if [[ $1 = 'install' ]]; then
  OPTIONS=${OPTIONS}' -boot once=d -cdrom '${CD_IMG}
fi

#echo $OPTIONS
qemu-system-x86_64 $OPTIONS
