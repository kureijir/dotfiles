" init.vim contains all of the initialization plugins for vim
" note that this has to be sourced second since dein needs to
" run its scripts first. This contains misc startup settings
" for vim

let $NVIM_TUI_ENABLE_TRUE_COLOR=1

" Enable syntax highlighting
syntax on

" Fixes backspace
"set backspace=indent,eol,start

" Enable line numbers
set number

" Enable line/column info at bottom
set ruler

" highlights current line
set cursorline

" remember last N commands
set history=100

" show last command entered in bottom bar
"set showcmd

" set the number of context lines above and below the cursor
set scrolloff=10

" Autoindentation
set autoindent
filetype indent plugin on

" Copies using system clipboard
set clipboard+=unnamedplus

" Tab = 2 spaces
set tabstop=2
set shiftwidth=2
" set sta
set expandtab
set sts=2 " softtabstop, makes spaces feel like tabs when deleting

" enable mouse support
{%@@ if profile == "thinkpadx240" @@%}
"set mouse=a mousemodel=popup
{%@@ else @@%}
set mouse=a mousemodel=popup
{%@@ endif @@%}

" markdown file recognition
autocmd BufNewFile,BufReadPost *.md set filetype=markdown
autocmd BufNewFile,BufReadPost *.md.html set filetype=markdown

" use ripgreg instead of grep
set grepprg=rg\ --vimgrep

" color scheme configuration
set background=light
set termguicolors
let g:gruvbox_italic=1
colorscheme gruvbox
"let g:gruvbox_contrast_light = 'hard'
"let g:gruvbox_contrast_dark = 'medium'
" disable background color and use that of terminal instead, might break GUI version of vim
highlight Normal guibg=none
highlight NonText guibg=none

" close vim if only window left is nerdtree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" crontab filetype tweak (the way vim normally saves files confuses crontab
" so this workaround allows for editing
au FileType crontab setlocal bkc=yes

" opening a new file when the current buffer has unsaved changes
" causes files to be hidden instead of closed
"set hidden

" Disable completion where available from ALE
" (not worth creating a separate file just for a one-liner)
let g:ale_completion_enabled = 0

" Only run linters named in ale_linters settings.
let g:ale_linters_explicit = 1

" terminal settings
autocmd BufWinEnter,WinEnter term://* startinsert
autocmd BufLeave term://* stopinsert

" markdown settings
let g:vim_markdown_conceal = 0
let g:vim_markdown_math = 0
