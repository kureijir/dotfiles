#!/bin/bash
#
# This script does this:
# launch an app if it isn't launched yet, 
# focus the app if it is launched but not focused,
# minimize the app if it is focused. 
#
# by desgua - 2012/04/29, last update: 2012/12/11

# Instructions
name=$(echo $0 | sed 's/.*\///')
if [ $# -ne 1 ]; then
  echo "
  This script does this:

  # launch an app if it isn't launched yet, 
  # focus the app if it is launched but not focused,
  # minimize the app if it is focused. 

  Usage: $name app

  Example: 

  $name gcalctool
  "
  exit 1

fi


# Let's check if the needed tools are instaled: 

if [ -z $(which xdotool) ] || [ -z $(which wmctrl) ]; then
  echo "Xdotool and Wmctrl is needed"
  exit 1
fi


# Check if the app is running

pid=$(pidof $1)

# If it isn't launched, then launch

if [ -z $pid ]; then
  $1 
  exit 0
else  

  # If it is launched then check if it is focused

  foc=$(xdotool getactivewindow getwindowpid)

  if [[ $pid == $foc ]]; then

    # if it is focused, then minimize
    xdotool getactivewindow windowminimize
    exit 0

  else

    # if it isn't focused then get focus
    wmctrl -x -R $1
    exit 0

  fi
fi

exit 0
