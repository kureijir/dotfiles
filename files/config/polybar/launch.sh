#!/usr/bin/env zsh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# set HOSTNAME environment variable to pass to the bars
export HOSTNAME=$(hostname -s)

# Launch bars
for bar in $@; do
  # echo $i
  polybar $bar &
done
# polybar bottom &
# polybar top &

echo "Bars launched..."
