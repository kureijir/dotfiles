#!/usr/bin/env bash

feh --no-fehbg --bg-fill '/home/kureiji/Wallpapers/Wallpaper-3B.jpg' &
compton -b
connman-gtk &
light-locker --no-late-locking --lock-on-suspend &
{%@@ if profile == "thinkpadx240" @@%}
~/.config/tiling-scripts/battery-popup.sh -ni dialog-error -L 15 -m 'Battery Low!' &
{%@@ endif @@%}
sleep 2s
