# vim: sw=4
# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from libqtile.config import Key, Screen, Group, ScratchPad, DropDown, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
import os
import subprocess

try:
    from typing import List  # noqa: F401
except ImportError:
    pass

home = os.path.expanduser('~')

## gruvbox-dark color scheme
#mycolor0  = '282828'        # normal contrast
##mycolor0  = '1d2021'        # hard contrast
##mycolor0  = '32302f'        # soft contrast
#mycolor1  = 'cc241d'
#mycolor2  = '98971a'
#mycolor3  = 'd79921'
#mycolor4  = '458588'
#mycolor5  = 'b16286'
#mycolor6  = '689d6a'
#mycolor7  = 'a89984'
#mycolor8  = '928374'
#mycolor9  = 'fb4934'
#mycolor10 = 'b8bb26'
#mycolor11 = 'fabd2f'
#mycolor12 = '83a598'
#mycolor13 = 'd3869b'
#mycolor14 = '8ec07c'
#mycolor15 = 'ebdbb2'

# gruvbox-light color scheme
#mycolor0  = 'fbf1c7'        # normal contrast
mycolor0  = 'f9f5d7'        # hard contrast
#mycolor0  = 'f2e5bc'        # soft contrast
mycolor1  = 'cc241d'
mycolor2  = '98971a'
mycolor3  = 'd79921'
mycolor4  = '458588'
mycolor5  = 'b16286'
mycolor6  = '689d6a'
mycolor7  = '7c6f64'
mycolor8  = '928374'
mycolor9  = '9d0006'
mycolor10 = '79740e'
mycolor11 = 'b57614'
mycolor12 = '076678'
mycolor13 = '8f3f71'
mycolor14 = '427b58'
mycolor15 = '3c3836'

#custom color variables
color_background = mycolor0
color_foreground = mycolor15
color_primary = mycolor4
color_secondary = mycolor7
color_alert = mycolor2

myhostname = subprocess.run(['hostname', '-s'],capture_output=True,text=True).stdout.rstrip()

@hook.subscribe.startup_once
def autostart():
    subprocess.call([home + '/.config/qtile/autostart.sh'])

@hook.subscribe.startup
def autostart_always():
    subprocess.call([home + '/.config/polybar/launch.sh', 'bottom'])

mod = "mod4"

# match windows to groups upon opening
# from https://github.com/qtile/qtile-examples/blob/master/aeronotix.py
@hook.subscribe.client_new
def grouper(window, windows={'Chromium': '3',
                              #'urxvt': ['music', 'weechat'],
                              'Opera': '5'}):

    """
    This function relies on the contentious feature of default arguments
    where upon function definition if the argument is a mutable datatype,
    then you are able to mutate the data held within that object.
    Current usage:
    {window_name: group_name}
    or for grouping windows to different groups you will need to have a
    list under the window-key with the order that you're starting the
    apps in.
    See the 'runner()' function for an example of using this method.
    Here:
    {window_name: [group_name1, group_name2]}
    Window name can be found via window.window.get_wm_class()
    """


    windowtype = window.window.get_wm_class()[0]

    # if the window is in our map
    if windowtype in windows.keys():

         # opening terminal applications gives
         # the window title the same name, this
         # means that we need to treat these apps
         # differently

         if windowtype != 'urxvt':
              window.togroup(windows[windowtype])
              windows.pop(windowtype)

         # if it's not on our special list,
         # we send it to the group and pop
         # that entry out the map
         else:
              try:
                   window.togroup(windows[windowtype][0])
                   windows[windowtype].pop(0)
              except IndexError:
                   pass


keys = [
    # Navigate windows and panes
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
    Key([mod, "control"], "j", lazy.layout.grow_down()),
    Key([mod, "control"], "k", lazy.layout.grow_up()),
    Key([mod, "control"], "h", lazy.layout.grow_left()),
    Key([mod, "control"], "l", lazy.layout.grow_right()),
    Key([mod], "b", lazy.layout.toggle_split()),
    Key([mod], "n", lazy.layout.normalize()),

    # Alternate keybinds for arrow keys
    Key([mod], "Down", lazy.layout.down()),
    Key([mod], "Up", lazy.layout.up()),
    Key([mod], "Left", lazy.layout.left()),
    Key([mod], "Right", lazy.layout.right()),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up()),
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right()),
    Key([mod, "control"], "Down", lazy.layout.grow_down()),
    Key([mod, "control"], "Up", lazy.layout.grow_up()),
    Key([mod, "control"], "Left", lazy.layout.grow_left()),
    Key([mod, "control"], "Right", lazy.layout.grow_right()),

    # Toggle between different layouts as defined below
    Key([mod], "w", lazy.next_layout()),

    # Kill a window
    Key([mod], "q", lazy.window.kill()),

    # System control
    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "p", lazy.spawn(home + "/.config/rofi/rofi-power -p 3 -l 'light-locker-command --lock' -e 'qtile-cmd -o cmd -f shutdown'")),
    # Key([mod, "control"], "p", lazy.spawn(home + "/.config/rofi/rofi-power")),
    # Key([mod, "control"], "e", lazy.shutdown()),
    # Key([mod, "control"], "p", lazy.spawn("systemctl poweroff")),
    # Key([mod, "control"], "s", lazy.spawn("systemctl suspend")),
    # Key([mod, "control"], "o", lazy.spawn("systemctl reboot")),
    # Key([mod, "control"], "l", lazy.spawn("light-locker-command --lock")),
    # Key([mod], "r", lazy.spawncmd()),
    Key([mod], "r", lazy.spawn("rofi -show run")),

    # Start some applications
    Key([mod], "d", lazy.spawn("rofi -show drun -show-icons")),
    Key([mod], "m", lazy.spawn("rofi -show window -show-icons")),
    Key([mod], "Return", lazy.spawn("alacritty")),

    # Backlight control
    Key([], "XF86MonBrightnessDown", lazy.spawn("xbacklight -dec 5")),
    Key([], "XF86MonBrightnessUp", lazy.spawn("xbacklight -inc 5")),

    # Audio volume control
    Key([], "XF86AudioRaiseVolume", lazy.spawn(home + "/.config/tiling-scripts/control/volume_pulseaudio inc")),
    Key([], "XF86AudioLowerVolume", lazy.spawn(home + "/.config/tiling-scripts/control/volume_pulseaudio dec")),
    Key([], "XF86AudioMute", lazy.spawn(home + "/.config/tiling-scripts/control/volume_pulseaudio toggle")),

    # MPD music daemon control
    Key([mod, "shift"], "p", lazy.spawn("mpc toggle")),
    Key([], "XF86AudioPlay", lazy.spawn("mpc toggle")),
    Key([], "XF86AudioPause", lazy.spawn("mpc toggle")),
    Key([mod, "shift"], "s", lazy.spawn("mpc stop")),
    Key([mod, "shift"], "n", lazy.spawn("mpc next")),
    Key([mod, "shift"], "t", lazy.spawn("mpc prev")),
    Key([mod, "shift"], "u", lazy.spawn("mpc volume +5")),
    Key([mod, "shift"], "d", lazy.spawn("mpc volume -5")),
]

my_groups = "1234567890"
groups = []
groups.append(ScratchPad("scratchpad", [
    DropDown("terminal", "alacritty",
        x=0,
        y=0,
        width=1,
        height=0.6,
        opacity=0.95,
        on_focus_lost_hide=True,
  )
]))
for i in my_groups: groups.append(Group(i))

keys.extend([
    Key([mod], 'grave', lazy.group['scratchpad'].dropdown_toggle('terminal')),
])
keys.extend([
    # toggle between two groups
    Key([mod], "Tab", lazy.screen.toggle_group()),

    # floating and fullscreen
    Key([mod, "shift"], "space", lazy.window.toggle_floating()),
    Key([mod], "f", lazy.window.toggle_fullscreen()),
])
for i in my_groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i, lazy.group[i].toscreen()),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i, lazy.window.togroup(i)),
    ])

layouts = [
    layout.Columns(
        num_stacks=2,
        border_width=0,
        margin=8
    ),
    layout.Max(),
]

widget_defaults = dict(
    font='NotoSansMono',
    fontsize=12,
    foreground=mycolor15,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.TextBox(myhostname),
                widget.GroupBox(
                    borderwidth=1,
                    padding=2,
                    spacing=1,
                    fontsize=10,
                    disable_drag=False,
                    use_mouse_wheel=False,
                    highlight_method='block',
                    active=mycolor15,
                    other_current_screen_border=mycolor4,
                    other_screen_border=mycolor4,
                    this_current_screen_border=mycolor4,
                    this_screen_border=mycolor4,
                    inactive=mycolor8,
                    urgent_border=mycolor3,
                    urgent_text=mycolor15,
                ),
                widget.Prompt(),
                widget.WindowName(),
                widget.Mpd(
                    do_color_progress=True,
                    foreground_progress=mycolor3,
                ),
                # widget.Mpd2(),
                # widget.TextBox("default config", name="default"),
                # widget.Systray(),
                # widget.Clock(format='%Y-%m-%d %a %I:%M %p'),
            ],
            22,
            background=mycolor0,
        ),
        # bottom=bar.Bar(
        #     [
        #         widget.Spacer(length=bar.STRETCH),
        #         widget.Net(
        #             interface='wlp2s0',
        #             foreground=mycolor12,
        #         ),
        #         widget.Wlan(
        #             interface='wlp2s0',
        #             format=' {essid}',
        #             foreground=mycolor6,
        #         ),
        #         widget.CPUGraph(
        #             border_color=mycolor5,
        #             graph_color=mycolor13,
        #             border_width=1,
        #             line_width=2,
        #         ),
        #         widget.ThermalSensor(
        #             foreground=mycolor11,
        #             foreground_alert=mycolor9,
        #         ),
        #         widget.Memory(
        #             fmt=' {MemUsed}M/{MemTotal}M',
        #             foreground=mycolor15,
        #             update_interval=30,
        #         ),
        #         widget.DF(
        #             format=' {f}{m}',
        #             visible_on_warn=False,
        #             foreground=mycolor10,
        #         ),
        #         widget.Backlight(
        #             backlight_name='intel_backlight',
        #             format=' {percent:2.0%}',
        #             foreground=mycolor3,
        #         ),
        #         widget.Volume(
        #             foreground=mycolor14,
        #         ),
        #         widget.Battery(
        #             error_message='No BAT',
        #             charge_char='',
        #             discharge_char='',
        #             foreground=mycolor11,
        #             low_foreground=mycolor9,
        #             low_threshold=0.15,
        #             format='{char} {percent:2.0%}',
        #         ),
        #         widget.Clock(
        #             format=' %a, %b %d, %Y',
        #         ),
        #         widget.Clock(
        #             format=' %H:%M',
        #             background=mycolor4,
        #         ),
        #         widget.Systray(icon_size=18),
        #     ],
        #     22,
        #     background=mycolor0,
        # ),
        # left=bar.Gap(2),
        # right=bar.Gap(2),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = False
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        {'wmclass': 'confirm'},
        {'wmclass': 'dialog'},
        {'wmclass': 'download'},
        {'wmclass': 'error'},
        {'wmclass': 'file_progress'},
        {'wmclass': 'notification'},
        {'wmclass': 'splash'},
        {'wmclass': 'toolbar'},
        {'wmclass': 'confirmreset'},  # gitk
        {'wmclass': 'makebranch'},  # gitk
        {'wmclass': 'maketag'},  # gitk
        {'wname': 'branchdialog'},  # gitk
        {'wname': 'pinentry'},  # GPG key password entry
        {'wmclass': 'ssh-askpass'},  # ssh-askpass
    ],
    border_width = 0,
)
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, github issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
