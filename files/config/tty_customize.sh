#!/usr/bin/env bash

if [ "$TERM" = "linux" ]; then
    #setfont Tamsyn8x16b
    setfont ter-118b

    ## gruvbox-dark
    #echo -en "\e]P0282828" #black normal contrast
    ##echo -en "\e]P01D2021" #black hard contrast
    ##echo -en "\e]P032302F" #black soft contrast
    #echo -en "\e]P8928374" #darkgrey
    #echo -en "\e]P1CC241D" #darkred
    #echo -en "\e]P9FB4934" #red
    #echo -en "\e]P298971A" #darkgreen
    #echo -en "\e]PAB8BB26" #green
    #echo -en "\e]P3D79921" #brown
    #echo -en "\e]PBFABD2F" #yellow
    #echo -en "\e]P4458588" #darkblue
    #echo -en "\e]PC83A598" #blue
    #echo -en "\e]P5B16286" #darkmagenta
    #echo -en "\e]PDD3869B" #magenta
    #echo -en "\e]P6689D6A" #darkcyan
    #echo -en "\e]PE8EC07C" #cyan
    #echo -en "\e]P7A89984" #lightgrey
    #echo -en "\e]PFEBDBB2" #white

    # gruvbox-light
    #echo -en "\e]P0FBF1C7" #black normal contrast
    echo -en "\e]P0F9F5D7" #black hard contrast
    #echo -en "\e]P0F2E5BC" #black soft contrast
    echo -en "\e]P8928374" #darkgrey
    echo -en "\e]P1CC241D" #darkred
    echo -en "\e]P99D0006" #red
    echo -en "\e]P298971A" #darkgreen
    echo -en "\e]PA79740E" #green
    echo -en "\e]P3D79921" #brown
    echo -en "\e]PBB57614" #yellow
    echo -en "\e]P4458588" #darkblue
    echo -en "\e]PC076678" #blue
    echo -en "\e]P5B16286" #darkmagenta
    echo -en "\e]PD8F3F71" #magenta
    echo -en "\e]P6689D6A" #darkcyan
    echo -en "\e]PE427B58" #cyan
    echo -en "\e]P77C6F64" #lightgrey
    echo -en "\e]PF3C3836" #white

    clear #for background artifacting
fi
