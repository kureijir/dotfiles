#!/usr/bin/env bash

# rotate.sh
#  ccw, cw, flip: relative movement
#  left, right, normal, inverted: absolute movement

# devices to manipulate
screen='DSI1'
touchscreen='04F3224A:00 04F3:2359'
touchpad='HID 0911:2188 Mouse'
keyboard='HID 0911:2188'

# disable keypad and touchpad on all but normal orientation
controlKeys=false

## runs cmd and greps output to find orientation
#orientationCmd='xrandr'
#orientationRE=/\s*#{$screen}\s+\w+\s+[x+\d]+\s+(|left|right|inverted)\s*\(/

# default direction
direction='left'
relativeDirection=''
transform=''


## get direction from cmd argument
if [[ -n $1 ]]; then
  case $1 in
    normal | left | right | inverted )
      direction=$1
      ;;
    cw | ccw | flip )
      relativeDirection=$1
      ;;
    *)
      echo Invalid argument \""$1"\"
      exit 1
      ;;
  esac
fi


## function definitions
doRotate(){
  if [[ -z $transform ]]; then
    echo "Can't rotate touchscreen into specified direction"
    exit 1
  fi
  xrandr --output $screen --rotate $direction
  xinput set-prop "$touchscreen" --type=float 'Coordinate Transformation Matrix' $transform
}

resolveRelativeDirection(){
  # get current direction from xrandr
  currentOrientation=$(xrandr --query | grep DSI1 | sed 's/ (normal.*//' | awk -F ' ' '{print $NF}')
  
  # turn direction into numbered state
  case $currentOrientation in
    normal)
      directionState=0
      ;;
    right)
      directionState=1
      ;;
    inverted)
      directionState=2
      ;;
    left)
      directionState=3
      ;;
  esac

  # change direction state according to specified relative direction
  case $relativeDirection in
    cw)
      directionState=$(($directionState + 1))
      ;;
    ccw)
      directionState=$(($directionState - 1))
      ;;
    flip)
      directionState=$(($directionState + 2))
      ;;
  esac
  directionState=$(($directionState % 4))

  # convert direction state back to direction
  case $directionState in
    0)
      direction='normal'
      ;;
    1)
      direction='right'
      ;;
    2)
      direction='inverted'
      ;;
    3)
      direction='left'
      ;;
  esac
}

getTransformationMatrix(){
  case $direction in
    normal)
      transform='1 0 0 0 1 0 0 0 1'
      ;;
    left)
      transform='0 -1 1 1 0 0 0 0 1'
      ;;
    right)
      transform='0 1 0 -1 0 1 0 0 1'
      ;;
    inverted)
      transform='-1 0 1 0 -1 1 0 0 1'
      ;;
  esac
}

## main script
if [[ -n $relativeDirection ]]; then
  resolveRelativeDirection
fi
getTransformationMatrix
doRotate
