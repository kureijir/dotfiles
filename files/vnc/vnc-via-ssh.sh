#!/bin/bash

HOSTS=($(grep "^Host " ~/.ssh/config | sed "s/^Host //"))

if [ -z $1 ]; then
  echo List of available hosts:
  for i in ${!HOSTS[*]}; do
    printf "%4d: %s\n" $i ${HOSTS[$i]}
  done

  read -p 'Which host do you want to connect to? ' CHOICE
else
  CHOICE=$1
fi

if [ $CHOICE -ge 0 ] && [ -n ${HOSTS[$CHOICE]} ]; then
  ssh -f ${HOSTS[$CHOICE]} sleep 10 ; vncviewer localhost:5901
else
  exit 1
fi
